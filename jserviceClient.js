const axios = require("axios")
const fs = require("fs")

module.exports = function(categoryIDs, categoriesFilePath) {
    const categoryResponsePromises = categoryIDs.map(id => 
        axios.get("http://jservice.io/api/category?id=" + id) // new array of promises from axios ids 
    )
    Promise.all(categoryResponsePromises)
        // wait for promise to be resolved 
        .then(categoryResponses => {
            const categories = categoryResponses.map(res => res.data)
            fs.writeFileSync(categoriesFilePath, JSON.stringify(categories))
            // cant have just as JSON.stringify b/c want response.data
        })   
}