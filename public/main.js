let jeopardyGrid = new JeopardyGrid({
    numOfColumns: 6,
    numOfRows: 5,
    targetElement: document.querySelector("main"),
    cellClasses: ["testCell"],
    score: 0,
    categoryIDs: [136, 249, 309, 105, 770, 67],
});

function reloadGrid() {
    const mainBoard = document.getElementById("main");
    mainBoard.innerHTML = "";
    jeopardyGrid = new JeopardyGrid({
        numOfColumns: 6,
        numOfRows: 5,
        targetElement: document.querySelector("main"),
        cellClasses: ["testCell"],
        score: 0,
        categoryIDs: [136, 249, 309, 105, 770, 67],
    });
    let scoreDivs = document.querySelectorAll('.scoreDiv');  
    for (var scoreDiv of scoreDivs) {
        scoreDiv.remove();
    }
}