class Grid {
    constructor (options) {
        this.cellConstructor = options.cellConstructor || Cell
        this.numOfRows = options.numOfRows;
        this.numOfColumns = options.numOfColumns;
        this.targetElement = options.targetElement || document.body;
        this.cellClasses = options.cellClasses || [];
        this.gridElement = this.createGridElement();
        this.rows = [];
        this.createRows();
        this.clickOnlyOnce = this.clickCell.bind(this);
        this.gridElement.addEventListener("click", this.clickOnlyOnce);
    }

    clickCell(event) {
        let clickedCell = event.target;
        if(!clickedCell.classList.contains("cell")) return;
        let rowIndex = Number(clickedCell.dataset.rowIndex);
        let colIndex = Number(clickedCell.dataset.colIndex);
        clickedCell.classList.add("clickedCell");
        clickedCell = this.findCellIndex(rowIndex, colIndex);
        this.clickCellForQandA(clickedCell);
    }

    clickCellForQandA(cell) {
        // override me, so does nothing here currently 
    }

    createGridElement() {
        const element = document.createElement("div");
        element.classList.add("grid");
        this.targetElement.appendChild(element);
        return element;
    }
    createRowElement(rowIndex) {
        const element = document.createElement("div");
        element.classList.add("row");
        element.dataset.rowIndex = rowIndex;
        this.gridElement.appendChild(element);
        if(!rowIndex == 0) console.log("test");
        return element;
    }
    createRows() {
        for (let rowIndex = 0; rowIndex < this.numOfRows; rowIndex++) {
            this.rows[rowIndex] = [];
            const rowElement = this.createRowElement(rowIndex);
            this.createCells(rowIndex, rowElement);
        }
    }
    createCells(rowIndex, rowElement) {
        for (let colIndex = 0; colIndex < this.numOfColumns; colIndex++) {
            const cell = new this.cellConstructor(rowIndex, colIndex, this.cellClasses);
            this.rows[rowIndex][colIndex] = cell;
            rowElement.appendChild(cell.element);
        }
    }
    findCellIndex(rowIndex, colIndex) {
        rowIndex = Number(rowIndex);
        colIndex = Number(colIndex);
        if(this.rows[rowIndex]) {
            if(this.rows[rowIndex][colIndex]) {
                return this.rows[rowIndex][colIndex];
            }
        }
        return null; 
    }
}