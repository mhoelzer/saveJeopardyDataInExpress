class JeopardyGrid extends Grid {
    constructor (options) {
        super(options);
        this.score = options.score;
        this.categoryIDs = options.categoryIDs;
        this.getCategories();
    };

    async getCategories() {
        const promisedHydratedObjects = this.categoryIDs.map(async categoryID => {     // .map makes new array from the results
            const response = await fetch('http://localhost:3000/api/category/' + categoryID);
            const hydratedObject = await response.json();
            return hydratedObject;
        });
        const categories = await Promise.all(promisedHydratedObjects);
        this.assignCluesToGrid(categories);
    };
    
    assignCluesToGrid(categories) {
        // nested loop for assigning a clue to every cell in the this.rows array (and to the DOM)
        for (let rowIndex = 0; rowIndex < this.numOfRows; rowIndex++) {
            for (let colIndex = 0; colIndex < this.numOfColumns; colIndex++) {
                const cell = this.rows[rowIndex][colIndex];
                const clue = categories[colIndex].clues[rowIndex];
                cell.element.innerText = `$${clue.value}` || `$${100}`;
                cell.pointValue = clue.value || 100;
                cell.question = clue.question || "NO QUESTION";
                cell.element.dataset.question = clue.question;
                cell.answer = clue.answer;
                console.log(cell.question);
                console.log(cell.answer);
            }
        }
    };
    
    clickCellForQandA(cell) {
        let userAnswer = prompt(cell.question).toLowerCase();
        let realAnswer = cell.answer.toLowerCase();
        let div = document.createElement("div");
        div.classList.add("scoreDiv");
        // // to not have a running score 
        // let scoreDivs = document.querySelectorAll('.scoreDiv');  
        // for (var scoreDiv of scoreDivs) {
        //     scoreDiv.remove();
        // }
        if (userAnswer == realAnswer) {
            this.score += cell.pointValue;
            // console.log(this.score)
            let divText = document.createTextNode(`Correct! You WON $${cell.pointValue}, and your total score is now $${this.score}!`);
            div.appendChild(divText);
            let place = document.getElementById("scoreHere");
            place.insertBefore(div, scoreHere.firstChild);
        } else {
            this.score -= cell.pointValue;
            // console.log(this.score)
            let divText = document.createTextNode(`Incorrect. The correct answer was: "${cell.answer}." You LOST $${cell.pointValue}, and your total score is now $${this.score}!`);
            div.appendChild(divText);
            let place = document.getElementById("scoreHere");
            place.insertBefore(div, scoreHere.firstChild);
        }
    };
}
