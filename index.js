const fs = require("fs"); 
const express = require("express");
const server = express();
const downloadCategoriesFromJService = require("./jserviceClient.js");
const port = 3000;
const categoryIDs = [136, 249, 309, 105, 770, 67]; 
const categoriesFilePath = "./categories.json";

downloadCategoriesFromJService(categoryIDs, categoriesFilePath); 

server.use(express.json()); 
server.use(express.static("public")); // frontend stuff; listens to slash by default 

server.get("/api/category/:id", (request, response) => {
    // pull from every time right now to pretend it's a database 
    const json = fs.readFileSync(categoriesFilePath); // string, not js object 
    const categories = JSON.parse(json);
    const requestedCategory = categories.find(category => category.id == request.params.id);
    console.log(`User req cat = ${request.params.id}`);
    response.send(requestedCategory);
});

server.listen(port, () => console.log(`Jeopardy server is listening on port ${port}!!!`));